#!/usr/bin/env python

import pygame

pygame.init()

YELLOW = pygame.Color("yellow")
GREEN = pygame.Color("green")
BLUE = pygame.Color("blue")
PURPLE = pygame.Color("purple")

width, height = (640, 480)
screen = pygame.display.set_mode((width, height))

class Ball(pygame.sprite.Sprite):
    radius = 25
    groups = []
    acceleration = 1
    def __init__(self, pos):
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.image = pygame.Surface((Ball.radius * 2, Ball.radius * 2))
        self.image.fill(YELLOW)
        self.image.convert_alpha()
        self.rect = self.image.get_rect()
        self.radius = Ball.radius
        self.velocity = 0
        pygame.draw.circle(self.image, BLUE, self.rect.center, self.radius, 0)
        self.rect.center = pos

    def update(self):
        if self.rect.top < height:
            self.rect.move_ip(0, self.velocity)
            bricks = pygame.sprite.spritecollide(self, static, False)
            if bricks:
                brick = bricks[0]
                self.rect.bottom = brick.rect.top
                self.velocity *= -0.9
            if 0 > self.velocity > -0.1:
                self.velocity = 0
            else:
                self.velocity += Ball.acceleration


class Brick(pygame.sprite.Sprite):
    width = 30
    groups = []
    def __init__(self, pos):
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.image = pygame.Surface((Brick.width * 2, Brick.width * 2))
        self.image.fill(GREEN)
        self.image.convert_alpha()
        self.rect = self.image.get_rect()
        pygame.draw.rect(self.image, PURPLE, self.rect)
        self.rect.center = pos

yellow_surface = pygame.Surface((width, height))
yellow_surface.fill(YELLOW)

allsprites = pygame.sprite.Group()
Ball.groups = allsprites

static = pygame.sprite.Group()
Brick.groups = allsprites, static

timer = pygame.time.Clock()
screen.blit(yellow_surface, (0, 0))
while 1:
    timer.tick(60)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            raise SystemExit("QUIT")
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            Ball(event.pos)
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 3:
            Brick(event.pos)
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_r:
            allsprites.empty()
            static.empty()
    allsprites.clear(screen, yellow_surface)
    allsprites.update()
    allsprites.draw(screen)
    pygame.display.update()
