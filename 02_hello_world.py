#!/usr/bin/env python

import pygame

pygame.init()

WHITE = pygame.Color(255, 255, 255)

width, height = (640, 480)
screen = pygame.display.set_mode((width, height))

# Define text to draw
font = pygame.font.Font(None, 50)
text = font.render("Hello, World!", True, WHITE)

# Draw text on screen
screen.blit(text, text.get_rect(centerx=width/2., centery=height/2.))
pygame.display.update()

while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            raise SystemExit("QUIT")
