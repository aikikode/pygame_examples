#!/usr/bin/env python

import pygame

pygame.init()

YELLOW = pygame.Color("yellow")
GREEN = pygame.Color("green")

width, height = (640, 480)
screen = pygame.display.set_mode((width, height))

yellow_surface = pygame.Surface((width, height))
yellow_surface.fill(YELLOW)

green_surface = pygame.Surface((width/2, height/2))
green_surface.fill(GREEN)

def draw_circle(pos, radius=width/8):
    pygame.draw.circle(yellow_surface, GREEN, pos, radius)
    pygame.draw.circle(green_surface, YELLOW, pos, radius)

pos = (0, 0)
move = False
while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            raise SystemExit("QUIT")
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            move = True
        elif event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            move = False
    if move:
        yellow_surface.fill(YELLOW)
        green_surface.fill(GREEN)
        pos = pygame.mouse.get_pos()
    draw_circle(pos)
    screen.blit(yellow_surface, (0, 0))
    screen.blit(green_surface, (0, 0))
    pygame.display.update()
