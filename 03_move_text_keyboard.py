#!/usr/bin/env python

import pygame

pygame.init()

WHITE = pygame.Color(255, 255, 255)
BLACK = pygame.Color(0, 0, 0)

width, height = (640, 480)
screen = pygame.display.set_mode((width, height))

def get_text(msg="Hello, World!"):
    font = pygame.font.Font(None, 50)
    text = font.render(msg, True, WHITE)
    return text

delta_x, delta_y = (0, 0)
step = 20
while 1:
    screen.fill(BLACK)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            raise SystemExit("QUIT")
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN:
            delta_y += step
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_UP:
            delta_y -= step
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
            delta_x -= step
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
            delta_x += step
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_r:
            delta_x, delta_y = (0, 0)
    text = get_text()
    screen.blit(text, text.get_rect(centerx=width/2. + delta_x, centery=height/2. + delta_y))
    pygame.display.update()
