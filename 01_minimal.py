#!/usr/bin/env python

import pygame

pygame.init()
screen = pygame.display.set_mode((640, 480))

while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            raise SystemExit("QUIT")
