#!/usr/bin/python

import pyglet
import pymunk
import pymunk.pyglet_util

window = pyglet.window.Window(width=600,height=600)

fps_display = pyglet.clock.ClockDisplay()

### Physics stuff
space = pymunk.Space()
space.gravity = (0.0, -900.0)

# Balls
balls = []

# Bricks
static_body = pymunk.Body()
bricks = []

class Ball(pymunk.Body):
    def __init__(self, pos, mass=10, radius=25):
        inertia = pymunk.moment_for_circle(mass, 0, radius, (0,0))
        pymunk.Body.__init__(self, mass, inertia)
        self.position = pos
        self.mass = mass
        self.radius = radius
        self.inertia = inertia
    def get_shape(self, elasticity=0.9):
        shape = pymunk.Circle(self, self.radius, (0,0))
        shape.elasticity = elasticity
        return shape

class Brick(pymunk.Poly):
    def __init__(self, pos, size=50):
        self.size = size
        center = pos
        rect_left = center[0] - size/2.
        rect_top = center[1] + size/2.
        pymunk.Poly.__init__(self, static_body, [
            (rect_left, rect_top),
            (rect_left + self.size, rect_top),
            (rect_left + self.size, rect_top - self.size),
            (rect_left, rect_top - self.size)])
        self.elasticity = 0.95

@window.event
def on_mouse_press(x, y, button, modifiers):
    if button == pyglet.window.mouse.LEFT:
        body = Ball((x, y))
        shape = body.get_shape()
        space.add(body, shape)
        balls.append(shape)
    elif button == pyglet.window.mouse.RIGHT:
        brick = Brick((x, y))
        space.add(brick)
        bricks.append(brick)

@window.event
def on_key_press(symbol, modifiers):
    if symbol == pyglet.window.key.R:
        for brick in bricks[:]:
            space.remove(brick)
            bricks.remove(brick)
    elif symbol == pyglet.window.key.SPACE:
        space.gravity *= -1

@window.event
def on_draw():
    pyglet.gl.glClearColor(0,0,0,1)
    window.clear()
    fps_display.draw()
    balls_to_remove = []
    for ball in balls:
        if ball.body.position.y < -100 or ball.body.position.y > 700:
            balls_to_remove.append(ball)
    for ball in balls_to_remove:
        space.remove(ball, ball.body)
        balls.remove(ball)
    pymunk.pyglet_util.draw(space)

def update(dt):
    dt = 1.0/60. #override dt to keep physics simulation stable
    space.step(dt)

pyglet.clock.schedule_interval(update, 1/60.)
pyglet.app.run()
