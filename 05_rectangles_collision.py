#!/usr/bin/env python

import pygame

pygame.init()

YELLOW = pygame.Color("yellow")
GREEN = pygame.Color("green")
BLUE = pygame.Color("blue")
PURPLE = pygame.Color("purple")

width, height = (640, 480)
screen = pygame.display.set_mode((width, height))

yellow_surface = pygame.Surface((width, height))
yellow_surface.fill(YELLOW)

static_rect = pygame.Rect((width/3, height/3), (width/3, height/3))
moving_rect = pygame.Rect((width*2/3, height*2/3), (width/6, height/6))

def draw_rects(pos):
    moving_rect.centerx, moving_rect.centery = pos
    pygame.draw.rect(yellow_surface, GREEN, moving_rect)
    if moving_rect.colliderect(static_rect):
        pygame.draw.rect(yellow_surface, PURPLE, static_rect)
    else:
        pygame.draw.rect(yellow_surface, BLUE, static_rect)

pos = (0, 0)
move = False
while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            raise SystemExit("QUIT")
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            move = True
        elif event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            move = False
    if move:
        yellow_surface.fill(YELLOW)
        pos = pygame.mouse.get_pos()
    draw_rects(pos)
    screen.blit(yellow_surface, (0, 0))
    pygame.display.update()
