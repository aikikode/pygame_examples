#!/usr/bin/env python

import pygame
from pygame.color import *
import pymunk as pm

def to_pygame(p):
    """Small hack to convert pymunk to pygame coordinates"""
    return int(p[0]), int(-p[1]+600)

pygame.init()
screen = pygame.display.set_mode((600, 600))
clock = pygame.time.Clock()

### Physics stuff
space = pm.Space()
space.gravity = (0, -900.0)

# Balls
balls = []

# Bricks
static_body = pm.Body()
bricks = []

class Ball(pm.Body):
    def __init__(self, pos, mass=10, radius=25):
        inertia = pm.moment_for_circle(mass, 0, radius, (0,0))
        pm.Body.__init__(self, mass, inertia)
        self.position = pos
        self.radius = radius
    def get_shape(self, elasticity=0.9):
        shape = pm.Circle(self, self.radius, (0,0))
        shape.elasticity = elasticity
        return shape

class Brick(pm.Poly):
    def __init__(self, pos, size=50):
        self.size = size
        center = pos
        rect_left = center[0] - size/2.
        rect_top = center[1] + size/2.
        pm.Poly.__init__(self, static_body, [
            (rect_left, rect_top),
            (rect_left + self.size, rect_top),
            (rect_left + self.size, rect_top - self.size),
            (rect_left, rect_top - self.size)])
        self.elasticity = 0.95

while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            raise SystemExit("QUIT")
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            body = Ball(to_pygame(event.pos))
            shape = body.get_shape()
            space.add(body, shape)
            balls.append(shape)
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 3:
            brick = Brick(to_pygame(event.pos))
            space.add(brick)
            bricks.append(brick)
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_r:
            for brick in bricks[:]:
                space.remove(brick)
                bricks.remove(brick)
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
            space.gravity *= -1

    ### Clear screen
    screen.fill(THECOLORS["white"])

    ### Draw stuff
    balls_to_remove = []
    for ball in balls:
        if ball.body.position.y < -100 or ball.body.position.y > 700:
            balls_to_remove.append(ball)
        p = to_pygame(ball.body.position)
        pygame.draw.circle(screen, THECOLORS["blue"], p, int(ball.radius), 2)

    for ball in balls_to_remove:
        space.remove(ball, ball.body)
        balls.remove(ball)

    for brick in bricks:
        rect_left, rect_top = brick.bb.left, brick.bb.top
        rect_size = brick.size
        pygame.draw.rect(screen, THECOLORS["lightgray"], pygame.Rect(to_pygame((rect_left, rect_top)),(rect_size, rect_size)))

    ### Update physics
    fps = 60.0
    space.step(1./fps)

    ### Flip screen
    pygame.display.flip()
    clock.tick(fps)
    pygame.display.set_caption("fps: " + str(clock.get_fps()))
