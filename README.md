Pygame snippets and examples
============================

Download pygame from [here](https://bitbucket.org/pygame/pygame) and visit
[official site](www.pygame.org) for API, details and examples.
